
using System.Collections.Generic;
using JetBrains.Annotations;
using NUnit.Framework;
[System.Serializable]
public class ItemsList 
{
    public List<Items> Items = new List<Items>();
}
