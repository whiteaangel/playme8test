
[System.Serializable]
public class Items
{
    public string PathToResources = "";
    public string PathToAvatar = "";
    public int ItemCount = 0;
    public string ItemName = "";
    public int MoneyCount = 0;
    public string UserName = "";
    public int UserLevel = 0;
}
