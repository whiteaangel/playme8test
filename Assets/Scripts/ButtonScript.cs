using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class ButtonScript : MonoBehaviour,IPointerDownHandler
{
    [SerializeField] private ScrollRectScript scrollRectScript;
    [SerializeField] private bool isLeftButton;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        if (isLeftButton)
        {
            scrollRectScript.ButtonLeftIsPressed();
        }
        else
        {
            scrollRectScript.ButtonRightIsPressed();
        }
    }
}
