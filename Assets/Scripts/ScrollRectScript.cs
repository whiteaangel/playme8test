using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollRectScript : MonoBehaviour
{
   public float speed = 0.001f;
   private ScrollRect _scrollRect;
   private bool _mouseDown, _buttonRight, _buttonLeft;

   private void Start()
   {
      _scrollRect = GetComponent<ScrollRect>();
   }

   private void Update()
   {
      if (_mouseDown)
      {
         if (_buttonRight)
         {
            ScrollRight();
         }
         else if (_buttonLeft)
         {
            ScrollLeft();
         }
      }
   }

   public void ButtonLeftIsPressed()
   {
      _mouseDown = true;
      _buttonLeft = true;
   }

   public void ButtonRightIsPressed()
   {
      _mouseDown = true;
      _buttonRight = true;
   }

   private void ScrollLeft()
   {
      if (Input.GetMouseButtonUp(0))
      {
         _mouseDown = false;
         _buttonLeft = false;
      }
      else
      {
         _scrollRect.horizontalNormalizedPosition -= speed;
      }
   }

   private void ScrollRight()
   {
      if (Input.GetMouseButtonUp(0))
      {
         _mouseDown = false;
         _buttonRight = false;
      }
      else
      {
         _scrollRect.horizontalNormalizedPosition += speed;
      }
   }
}
