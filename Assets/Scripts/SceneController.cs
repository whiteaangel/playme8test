using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    [SerializeField] private RectTransform content;
    [SerializeField] private int objectCount = 100;
    private ItemsList _itemsList = new ItemsList();

    private void Start()
    {
        ReadJson("Json/Items");
        GenerateObjects();
    }

    private void ReadJson(string path)
    {
        TextAsset json = Resources.Load(path) as TextAsset;
        if(json == null)
            return;
        _itemsList = JsonUtility.FromJson<ItemsList>(json.text);
    }

    private void GenerateObjects()
    {
        for (var i = 0; i < objectCount; i++)
        {
            var instance = Instantiate(Resources.Load("UI/ItemPanel"), content, false) as GameObject;
            InitializeItem(instance,_itemsList.Items[i]);
        }
    }

    private void InitializeItem(GameObject instance, Items it)
    {
        instance.transform.GetChild(2).GetComponent<Image>().sprite =
            Resources.Load<Sprite>(it.PathToResources);
        StartCoroutine(LoadImageFromWeb(it.PathToAvatar,instance.transform.GetChild(6).GetComponent<Image>()));
        instance.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "x" + it.ItemCount.ToString();
        instance.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = it.ItemName;
        instance.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = it.MoneyCount.ToString();
        instance.transform.GetChild(7).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = it.UserLevel.ToString();
        instance.transform.GetChild(10).GetComponent<TextMeshProUGUI>().text = it.UserName;
    }

    private IEnumerator LoadImageFromWeb(string url,Image img)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();
        if(request.isNetworkError || request.isHttpError) 
            Debug.Log(request.error);
        else
        {
            Texture2D texture2D;
            texture2D = ((DownloadHandlerTexture) request.downloadHandler).texture;
            img.sprite = Sprite.Create(texture2D,new Rect(0,0, texture2D.width,texture2D.height), new Vector2(0,0));
        }
    }
}
